var app = angular.module("celtenApp",[]);

	function getUsuarioPorLogueo($scope, $http){
		$http({
			method:"GET",
			url:"getUsuarioPorLogueo"
		}).success(function(data){
			$scope.usuario = data;		
		}).error(function(data){
			alert("Error");
		});
	}
	
	app.controller("SideNavCtrl", ['$scope','$http', getUsuarioPorLogueo]);	