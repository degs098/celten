<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<!DOCTYPE html>
<html ng-app="celtenApp">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Inicio - Celten S.A. | Celten</title>

	<link rel="stylesheet" type="text/css" href="<c:url value="resources/css/main.css"/>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">	
	
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>	
	<script type="text/javascript" src="<c:url value="resources/angularResources/controllers/sideNavController.js"/>"></script>	
</head>
<body>

	<jsp:include page="decorador/header.jsp"/>	
	<jsp:include page="decorador/sideNav.jsp"/>
	
	<div class="dashboardContainer">
		<div class="main">
			<div class="row grey lighten-3">
				<div class="headerContainer col s12">
					<div class="iconDivContainer">
						<div class="iconDiv left">
							<i class="material-icons grey-text text-darken-2 circle small">&#xE88A;</i>
						</div>					
						<p class="flow-text grey-text text-darken-2">						
							Inicio
						</p>
					</div>					
				</div>				
			</div>
			<div class="row">
				<div class="quickAccessContainer col s8">
					<div class="iconDivContainer">
						<div class="iconDiv left">
							<i class="material-icons grey-text text-darken-2 circle small">&#xE87A;</i>							
						</div>					
						<p class="flow-text grey-text text-darken-2">						
							Acceso R�pido
						</p>
					</div>

					<div class="quickAccessInternalContainer">
						<div class="agendaAccess card-panel blue col offset-s1 quickAccess waves-effect">
							<i class="material-icons medium white-text text-lighten-2">&#xE916;</i>
							<span class="flow-text white-text text-darken-2">Mi Agenda</span>
						</div>
						<a href="<c:url value="/perfil"/>">						
							<div class="perfilAccess card-panel blue col quickAccess waves-effect">
								<i class="material-icons medium white-text text-lighten-2">&#xE853;</i>
								<span class="flow-text white-text text-darken-2">Mi Perfil</span>
							</div>					
						</a>
						<div class="agendaAccess card-panel blue col quickAccess waves-effect">
							<i class="material-icons medium white-text text-lighten-2">&#xE80B;</i>
							<span class="flow-text white-text text-darken-2">Publicaciones</span>
						</div>		
						<div class="agendaAccess card-panel blue col quickAccess waves-effect">
							<i class="material-icons medium white-text text-lighten-2">&#xE862;</i>
							<span class="flow-text white-text text-darken-2">Mis Tareas</span>
						</div>
						<div class="agendaAccess card-panel blue col quickAccess waves-effect">
							<i class="material-icons medium white-text text-lighten-2">&#xE0C9;</i>						
							<span class="flow-text white-text text-darken-2">Chat</span>
						</div>
					</div>

				</div>
			</div>
			<div class="row grey lighten-3">
				<div class="taskContainer col s8">
					<div class="iconDivContainer">
						<div class="iconDiv left">
							<i class="material-icons grey-text text-darken-2 circle small">&#xE86C;</i>
						</div>					
						<p class="flow-text grey-text text-darken-2">
							Tareas Para Hoy
						</p>
					</div>
					<c:if test="${not empty tareasUsuario}">
						<div class="listTask col offset-s1">
							<div class="collection">
								<c:forEach var="tasks" items="${tareasUsuario}">
									<a id="${tasks.getId()}" class="collection-item row waves-effect">
										<span class="col s11 blue-text">
											${tasks.getDescripcion()}
										</span>
										<span class="badge col s1">
											<i class="material-icons grey-text text-darken-2 small">&#xE5CD;</i>
											<i class="material-icons grey-text text-darken-2 small hidden" >&#xE5CA;</i>
										</span>
									</a>
								</c:forEach>
							</div>
						</div>
					</c:if>
				</div>
			</div>
		</div>
		<div class="onlineContainer col s4">										
			<ul class="collection onlineTitle">
				<li class="collection-item">
					<div>
						<span><strong>En L�nea</strong></span>
						<i class="material-icons tiny right blue-text text-darken-1">&#xE061;</i>
					</div>
				</li>
			</ul>
			<div class="onlineScroll">
				<ul class="collection onlineContent">
					<a class="dropdown-button" href="#!" data-activates="teamFrontend">					
						<li class="collection-item">
							<i class="grey-text text-darken-2 material-icons left onlineGroupTitle">&#xE3AE;</i>
							<span><strong>Equipo Frontend</strong></span>						
							<i class="grey-text text-darken-2 material-icons right">&#xE313;</i>	
							

							<ul id="teamFrontend" class="dropdown-content">
								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/caliche.jpg"/>">
									<span class="title">Andres Serna</span>							
								</li>
								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/samuel.jpg"/>">
									<span class="title">Samuel Vergara</span>
								</li>
							</ul>
						</li>
					</a>
					<a class="dropdown-button" href="#!" data-activates="teamBackend">
						<li class="collection-item">
							<i class="grey-text text-darken-2 material-icons left onlineGroupTitle">&#xE312;</i>
							<span><strong>Equipo Backend</strong></span>
							<i class="grey-text text-darken-2 material-icons right">&#xE313;</i>						
							
							<ul id="teamBackend" class="dropdown-content">					
								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/pernett.jpg"/>">
									<span class="title">Sebas Pernett</span>							
								</li>

								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/tolosita.jpg"/>">
									<span class="title">Juan Tolosa</span>
								</li>

								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/tolosita.jpg"/>">
									<span class="title">Juan Tolosa</span>
								</li>

								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/tolosita.jpg"/>">
									<span class="title">Juan Tolosa</span>
								</li>

								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/tolosita.jpg"/>">
									<span class="title">Juan Tolosa</span>
								</li>
							</ul>
						</li>
					</a>
				</ul>
			</div>
		</div>	
	</div>	
          
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script type="text/javascript" src="<c:url value="resources/js/global.js"/>"></script>	
	<script type="text/javascript" src="<c:url value="resources/js/hammer.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/jquery.hammer.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/velocity.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/sideNav.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/waves.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/forms.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/dropdown.js"/>"></script>

	<script type="text/javascript" src="<c:url value="resources/js/dashboard/animateTask.js"/>"></script>

	
	<script type="text/javascript">
		function formSubmitLogout() {
			document.getElementById("logoutForm").submit();
		}
		jQuery(".button-collapse").sideNav();
		jQuery('.button-collapse').click(function(){
			this.sideNav('show');
		});
	</script>

</body>
</html>