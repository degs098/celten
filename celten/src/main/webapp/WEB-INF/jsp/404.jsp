<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" session="true" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>P�gina no encontrada - Celten S.A. | Celten</title>

	<link rel="stylesheet" type="text/css" href="<c:url value="resources/css/main.css"/>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>

	<jsp:include page="decorador/header.jsp"/>

  	<section id="PageNotFoundContent" class="main fullscreen">  		
  			<div class="row valign-wrapper">
  				<div class="col s12 center valign">
  					<h1 class="center">Oops! Error 404</h1>
  					<img src="<c:url value="resources/img/404Guy.png"/>">
  					<p class="flow-text center">Creo que te has perdido</p>
  					<p class="flow-text center">Hemos buscado en todos lados, pero tal parece que la p�gina que buscas no existe.</p>
  					<div class="center">
  						<a class="waves-effect waves-light btn center" onclick="goBack()"><i class="material-icons left">&#xE314;</i>Ir atras</a>
  					</div>
  				</div>
  			</div>
  	</section>
  	
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	
	<script type="text/javascript" src="<c:url value="resources/js/global.js"/>"></script>	
	
	<script type="text/javascript" src="<c:url value="resources/js/404/history.back.js"/>"></script>

</body>
</html>