<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Mis Tareas - Celten S.A. | Celten</title>

	<link rel="stylesheet" type="text/css" href="<c:url value="resources/css/main.css"/>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">	
</head>
<body>

	<header id="header">
		<div class="navbar-fixed">
			<nav>
				<div class="nav-wrapper">
					<div class="brand-logo center">
						<img src="<c:url value="resources/img/logo.png"/>"> <span>Celten</span>
					</div>
					<div class="right">
						<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
					</div>
				</div>
			</nav>
		</div>
	</header>
	
	<form action="logout" method="POST" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>

	<ul id="slide-out" class="side-nav fixed">
		<li>
			<div class="userView">
				<img class="background" src="<c:url value="resources/img/intro.jpg"/>">
				<img class="circle" src="<c:url value="resources/img/Perfil/daniel.jpg"/>">
				<a href="#"><span class="white-text name">${nombreUsuario}</span></a>
				<a href="#"><span class="white-text email">${email}</span></a>
			</div>
		</li>	    
		<li><a href="<c:url value="/dashboard"/>" class="waves-effect"><i class="material-icons">home</i>Inicio</a></li>
		<li><div class="divider"></div></li>
		<li><a href="<c:url value="/perfil"/>" class="waves-effect"><i class="material-icons">&#xE853;</i>Mi perfil</a></li>
		<li><div class="divider"></div></li>
		<li><a href="javascript:formSubmitLogout()" class="waves-effect"><i class="material-icons">&#xE879;</i>Cerrar sesi�n</a></li>	    
	</ul>  
	
	<div class="onlineContainer col s4">										
			<ul class="collection onlineTitle">
				<li class="collection-item">
					<div>
						<span><strong>En L�nea</strong></span>
						<i class="material-icons tiny right blue-text text-darken-1">&#xE061;</i>
					</div>
				</li>
			</ul>
			<div class="onlineScroll">
				<ul class="collection onlineContent">
					<a class="dropdown-button" href="#!" data-activates="teamFrontend">					
						<li class="collection-item">
							<i class="grey-text text-darken-2 material-icons left onlineGroupTitle">&#xE3AE;</i>
							<span><strong>Equipo Frontend</strong></span>						
							<i class="grey-text text-darken-2 material-icons right">&#xE313;</i>	
							

							<ul id="teamFrontend" class="dropdown-content">
								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/caliche.jpg"/>">
									<span class="title">Andres Serna</span>							
								</li>
								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/samuel.jpg"/>">
									<span class="title">Samuel Vergara</span>
								</li>
							</ul>
						</li>
					</a>
					<a class="dropdown-button" href="#!" data-activates="teamBackend">
						<li class="collection-item">
							<i class="grey-text text-darken-2 material-icons left onlineGroupTitle">&#xE312;</i>
							<span><strong>Equipo Backend</strong></span>
							<i class="grey-text text-darken-2 material-icons right">&#xE313;</i>						
							
							<ul id="teamBackend" class="dropdown-content">					
								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/pernett.jpg"/>">
									<span class="title">Sebas Pernett</span>							
								</li>

								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/tolosita.jpg"/>">
									<span class="title">Juan Tolosa</span>
								</li>

								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/tolosita.jpg"/>">
									<span class="title">Juan Tolosa</span>
								</li>

								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/tolosita.jpg"/>">
									<span class="title">Juan Tolosa</span>
								</li>

								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/tolosita.jpg"/>">
									<span class="title">Juan Tolosa</span>
								</li>
							</ul>
						</li>
					</a>
				</ul>
			</div>
		</div>	
	
	<div class="dashboardContainer">
		<div class="main">
			<div class="row grey lighten-3">
				<div class="headerContainer col s12">
					<div class="iconDivContainer">
						<div class="iconDiv left">
							<i class="material-icons grey-text text-darken-2 circle small">&#xE86C;</i>
						</div>					
						<p class="flow-text grey-text text-darken-2">						
							Mis Tareas
						</p>
					</div>					
				</div>				
			</div>
			<div class="row">
				<div class="taskContainer col s9">
					<div class="iconDivContainer row">
						<a class="btn-floating btn-large waves-effect blue waves-light tooltipped iconPlus" data-position="right" data-delay="50" data-tooltip="Agregar tarea"><i class="material-icons left white-text">add</i></a>
						<a class="btn-floating btn-large waves-effect blue waves-light tooltipped right iconCalendar" data-position="left" data-delay="50" data-tooltip="Consulta otro dia"><i class="material-icons left white-text"">date_range</i></a>
					</div>
					<div class="inTask col s6 offset-s1">
						<ul class="collection ">
							<li class="collection-item grey lighten-3 row">
								<div class="center-align">
									<span class="grey-text text-darken-2 ">En Proceso</span>
								</div>
							</li>
						</ul>
						<ul class="collection taskContent z-depth-1 grey-text text-darken-3">
							<li class="collection-item row">
								<div class="col s1 checkTask">
									<input type="checkbox" class="filled-in" id="1" />
									<label for="1" class="blue-text"></label>
								</div>
								<span class="col s11 contentCheck">
									Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...
								</span>
							</li>
							<li class="collection-item row">
								<div class="col s1 checkTask">
									<input type="checkbox" class="filled-in" id="2" />
									<label for="2"></label>
								</div>
								<span class="col s11 contentCheck">
									Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...
								</span>
							</li>
							<li class="collection-item row">
								<div class="col s1 checkTask">
									<input type="checkbox" class="filled-in" id="3" />
									<label for="3"></label>
								</div>
								<span class="col s11 contentCheck">
									Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...
								</span>
							</li>
							<li class="collection-item row">
								<div class="col s1 checkTask">
									<input type="checkbox" class="filled-in" id="4" />
									<label for="4"></label>
								</div>
								<span class="col s11 contentCheck">
									Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...
								</span>
							</li>
						</ul>
					</div>
					<div class="inTask col s6 offset-s1">
						<ul class="collection">
							<li class="collection-item grey lighten-3">
								<div class="center-align">
									<span class="grey-text text-darken-2 ">Terminadas</span>
								</div>
							</li>
						</ul>
						<ul class="collection taskContent z-depth-1 grey-text text-darken-3">
							<li class="collection-item row">
								<div class="col s1 checkTask">
									<input type="checkbox" class="filled-in" id="5" checked="checked"/>
									<label for="5"></label>
								</div>
								<span class="col s11 contentCheck">
									Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...
								</span>
							</li>
							<li class="collection-item row">
								<div class="col s1 checkTask">
									<input type="checkbox" class="filled-in" id="6" checked="checked"/>
									<label for="6"></label>
								</div>
								<span class="col s11 contentCheck">
									Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...
								</span>
							</li>
							<li class="collection-item row">
								<div class="col s1 checkTask">
									<input type="checkbox" class="filled-in" id="7" checked="checked"/>
									<label for="7"></label>
								</div>
								<span class="col s11 contentCheck">
									Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...
								</span>
							</li>
							<li class="collection-item row">
								<div class="col s1 checkTask">
									<input type="checkbox" class="filled-in" id="8" checked="checked"/>
									<label for="8"></label>
								</div>
								<span class="col s11 contentCheck">
									Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...
								</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>	
	
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script type="text/javascript" src="<c:url value="resources/js/global.js"/>"></script>	
	<script type="text/javascript" src="<c:url value="resources/js/hammer.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/jquery.hammer.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/velocity.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/sideNav.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/waves.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/forms.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/dropdown.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/tooltip.js"/>"></script>

	<script type="text/javascript" src="<c:url value="resources/js/perfil/preview.image.js"/>"></script>

	<script type="text/javascript">
		jQuery(".button-collapse").sideNav();
		jQuery('.button-collapse').click(function(){
			this.sideNav('show');
		});
		
		function formSubmitLogout() {
			document.getElementById("logoutForm").submit();
		}		
	</script>
</body>
</html>