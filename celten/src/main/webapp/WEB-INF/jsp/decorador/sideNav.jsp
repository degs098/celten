<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
	
	<form action="logout" method="POST" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>

	<ul id="slide-out" class="side-nav fixed" ng-controller="SideNavCtrl">
		<li>
			<div class="userView">
				<img class="background" src="<c:url value="resources/img/intro.jpg"/>">
				<img class="circle" src="<c:url value="data:image/jpeg;base64,${imagenPerfil}"/>">				
					<a href="#"><span class="white-text name">{{usuario.nombre}} {{usuario.apellido}}</span></a>
					<a href="#"><span class="white-text email">{{usuario.email}}</span></a>											
			</div>
		</li>	    
		<li><a href="<c:url value="/dashboard"/>" class="waves-effect"><i class="material-icons">home</i>Inicio</a></li>
		<li><div class="divider"></div></li>
		<li><a href="<c:url value="/perfil"/>" class="waves-effect"><i class="material-icons">&#xE853;</i>Mi perfil</a></li>
		<li><div class="divider"></div></li>
		<li><a href="javascript:formSubmitLogout()" class="waves-effect"><i class="material-icons">&#xE879;</i>Cerrar sesi�n</a></li>	    
	</ul> 
