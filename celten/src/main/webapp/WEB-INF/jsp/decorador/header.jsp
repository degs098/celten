<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<header id="header">
	<div class="navbar-fixed">
		<nav>
			<div class="nav-wrapper">
				<div class="brand-logo center">
					<img src="<c:url value="resources/img/logo.png"/>"> <span>Celten</span>
				</div>
				<div class="right">
					<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
				</div>
			</div>
		</nav>
	</div>
</header>