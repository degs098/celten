<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" session="true" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Celten S.A. | Celten</title>

	<link rel="stylesheet" type="text/css" href="<c:url value="resources/css/main.css"/>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">	
		
</head>
<body>

	<jsp:include page="decorador/header.jsp"/>

  	<div class="preloader-wrapper big active">
	    <div class="spinner-layer spinner-blue-only">
	      <div class="circle-clipper left">
	        <div class="circle"></div>
	      </div><div class="gap-patch">
	        <div class="circle"></div>
	      </div><div class="circle-clipper right">
	        <div class="circle"></div>
	      </div>
	    </div>
  	</div>	  	
  	
	<div id="modal1" class="modal valign">
		<div class="modal-content">
			<div class="row">
				<div class="col s12">													
					<div class="row">
				    	<div class="col s12">
				    		<p class="center flow-text"><strong>�Empecemos!</strong></p>				    			
				    	</div>
				    </div>
				    <div class="divider"></div>				    
				    <div id="modrv" class="row valign-wrapper">
				    	<div id="modrvcont" class="col s6 valign">		
					    	<form:form id="loginForm" name="loginForm" action="login" method="POST" >				    	
						    	<c:if test="${not empty error}">
						    		<div class="card-panel red lighten-1">
						    			<span class="white-text">${error}</span>
						    		</div>
						    	</c:if>
						    	<c:if test="${not empty logoutMessage}">
						    		<div class="card-panel green lighten-1">
						    			<span class="white-text">${logoutMessage}</span>
						    		</div>
						    	</c:if>						    									
						    	<div class="row">
						    		<div class="input-field col s12">
						    			<i class="material-icons prefix">account_circle</i>
						    			<input class="validate" type="text" id="username" name="username">
						    			<label for="username">Usuario</label>
						    		</div>
						    	</div>
						    	<div class="row">
						    		<div class="input-field col s12">
						    			<i class="material-icons prefix">&#xE0DA;</i>
						    			<input class="validate" type="password" id="password" name="password">
						    			<label for="password">Contrase�a</label>
						    		</div>
						    	</div>
						    	<div class="row">
						    		<div class="input-field center">
						    			<button class="waves-effect waves-light btn">Iniciar Sesi�n</button>
						    		</div>
						    	</div>
						    	<div class="row">
						    		<div class="col s12 center">
						    			<a class="modal-action modal-close modal-trigger" href="#modal2">�Olvidaste tu contrase�a?</a>
						    		</div>
						    	</div>	
						    	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />					    	  
						    </form:form>	
					    </div>
					    <div class="col s6">
					    	<img src="<c:url value="resources/img/graphicDesignerDeskV2.png"/>">
					    </div>
					</div>					
				</div>
			</div>		   		   
		</div>
		<div class="modal-footer">
			<p class="left">Celten <i class="tiny material-icons">&#xE90C;</i>2016</p>
		</div>
	</div>

	<div id="modal2" class="modal">
	    <div class="modal-content">
	      <div class="row">
				<div class="col s12">													
					<div class="row">
				    	<div class="col s12">
				    		<p class="center flow-text"><strong>Recuperar Contrase�a</strong></p> 			
				    	</div>
				    </div>
				    <div class="divider"></div>	
				    <br>
				    <p>Por favor ingresa el correo asociado al usuario.</p>	
				    <br>		    
				    <div class="row valign-wrapper">
				    	<div class="col s12 valign">
					    	<form:form action="" method="POST">				    	
						    	<div class="row" >						    		
						    		<div class="input-field col s12" >
						    			<i class="material-icons prefix">&#xE0BE;</i>
						    			<input class="validate" type="email" id="email" name="email">
						    			<label for="email">Escribe tu correo</label>
						    		</div>
						    	</div>
						    </form:form>	
					    </div>
					</div>					
				</div>
			</div>
	    </div>
	    <div class="modal-footer">
	    		<a class="waves-effect waves-light btn" href="">Enviar<i class="material-icons right">&#xE163;</i></a>	 			    	
	    </div>
  </div>

	<section id="intro" class="main fullscreen">
		<div class="section">
			<div class="container">	
				<br>			
				<h1 id="hello"><strong>�Qu� tal?</strong></h1>
				
				<br>
				<p id="slogan" class="flow-text"><strong>Celten</strong>, un espacio para aprender de lo que m�s nos gusta hacer.</p>
				<br>

				<div class="row">
					<div id="getStarted" class="col s6">
						<a id="getStartedButton" class="modal-trigger waves-effect waves-light btn-large" data-target="modal1">�Empecemos!</a>
					</div>

					<div id="contactUs" class="col s6">
						<a id="contactUsButton" class="waves-effect waves-light btn-large"><i class="material-icons right">&#xE313;</i>Con�cenos</a>
					</div>
				</div>
			</div>	
	  	</div>
	</section>	

	<section id="one" class="main fullscreen valign-wrapper">
		<div class="section white valign">
			<div class="container hide">
				<div class="row">
					<div class="col s4 fade">
						<i class="extra-large material-icons">group_work</i>
						<p class="flow-text"><strong>Fomentamos el trabajo en equipo para proyectos de gran calidad.</strong></p>
						<p class="light">En Celten, el trabajo grupal guiado por un excelente liderazgo es esencial para el camino hacia el �xito tanto individual como colectivo.</p>
					</div>

					<div class="col s4 fade">
						<i class="extra-large material-icons">group</i>
						<p class="flow-text"><strong>Pensamos siempre en la necesidad del usuario final.</strong></p>
						<p class="light">La calidad de nuestros proyectos se mide dependiendo de la satisfacci�n de nuestros clientes y por supuesto, de nuestros usuarios.</p>
					</div>

					<div class="col s4 fade">
						<i class="extra-large material-icons">trending_up</i>
						<p class="flow-text"><strong>Mejoramos cada vez m�s nuestras m�tricas.</strong></p>
						<p class="light">La satisfacci�n de nuestros usuarios es medida por medio de indicadores estad�sticos de los cuales dependemos para la mejora constante.</p>
					</div>
				</div>													
			</div>

			<div class="show">
				<ul class="collapsible" data-collapsible="accordion">
				    <li>
				    	<div class="collapsible-header">
					    	<div class="row">
					    		<div class="col s1"><i class="extra-large material-icons">group_work</i></div>
					    		<div class="col s10">
					    			<p class="flow-text"><strong>Fomentamos el trabajo en equipo para proyectos de gran calidad.</strong></p>
					    		</div>
					    		<div class="col s1">
					    			<i class="medium material-icons arrow-down">&#xE313;</i>
					    		</div>
					    	</div>
				    	</div>
				    	<div class="collapsible-body">
							<p class="light">En Celten, el trabajo grupal guiado por un excelente liderazgo es esencial para el camino hacia el �xito tanto individual como colectivo.</p>
						</div>
				    </li>
				    <li>
				    	<div class="collapsible-header">
				    		<div class="row">
					    		<div class="col s1"><i class="extra-large material-icons">group</i></div>
					    		<div class="col s10">
					    			<p class="flow-text"><strong>Pensamos siempre en la necesidad del usuario final.</strong></p>
					    		</div>
					    		<div class="col s1">
					    			<i class="medium material-icons arrow-down">&#xE313;</i>
					    		</div>
					    	</div>
				      	</div>
				      	<div class="collapsible-body">
				      		<p class="light">La calidad de nuestros proyectos se mide dependiendo de la satisfacci�n de nuestros clientes y por supuesto, de nuestros usuarios.</p>
				      	</div>
				    </li>
				    <li>
				      	<div class="collapsible-header">
							<div class="row">
					    		<div class="col s1"><i class="extra-large material-icons">trending_up</i></div>
					    		<div class="col s10">
					    			<p class="flow-text"><strong>Mejoramos cada vez m�s nuestras m�tricas.</strong></p>
					    		</div>
					    		<div class="col s1">
					    			<i class="medium material-icons arrow-down">&#xE313;</i>
					    		</div>
					    	</div>
				      	</div>
				      	<div class="collapsible-body">
				      		<p class="light">La satisfacci�n de nuestros usuarios es medida por medio de indicadores estad�sticos de los cuales dependemos para la mejora constante.</p>
				      	</div>
				    </li>
				</ul>
			</div>
			
			<div class="center">
				<i id="goToTwo" class="medium material-icons arrow-down">&#xE313;</i>	
			</div>
		</div>
	</section>

	<section id="two" class="main fullscreen valign-wrapper">
		<div class="section valign">
			<div class="container valign-wrapper">				
				<div class="row valign">
					<div class="col s4 left frontendGuyImage">
						<img src="<c:url value="resources/img/frontend-guy.PNG"/>">
					</div>

					<div id="context" class="col s7 right personalText">
						<h1 class="flow-text"><strong>�Contamos con el personal m�s gomoso en lo que hacen!</strong></h1>
						<p class="flow-text">�Aqu� encontrar�s a personas carism�ticas, llenas de motivaci�n y energ�a para convertir en �xito los proyectos enfocados a adaptarse a las nuevas tendencias de las tecnolog�as de informaci�n!</p>
					</div>
				</div>				
			</div>
			<div class="center">
				<i id="goToThree" class="medium material-icons arrow-down">&#xE313;</i>	
			</div>
		</div>
	</section>

	<section id="three" class="main fullscreen">
			<div class="container">
				<div class="row">
					<div class="col s6 center facebook scrollflow -slide-right -opacity" >
						<img src="<c:url value="resources/img/facebook.png"/>">
						<p class="light">�Danos Me Gusta en Facebook! </p>
						<a class="waves-effect waves-light btn"> <i class="icon-thumbs-up"></i><span> Like a Celten </span></a>
					</div>
					<div class="col s6 center twitter scrollflow -slide-left -opacity">
						<img src="<c:url value="resources/img/twitter.png"/>">
						<p class="light">�S�guenos en Twitter!</p>
						<a href="https://twitter.com/intent/follow?%2F&ref_src=twsrc%5Etfw&region=follow_link&screen_name=WeAreCelten&tw_p=followbutton" class="waves-effect waves-light btn" data-show-count="false" data-lang="es" data-size="large" data-dnt="true">
						<i class="icon-twitter"></i>Sigue a @WeAreCelten </a> 
					</div>
				</div>
			</div>					
	</section>

	<footer id="four" class="page-footer">
		<div class="footer1">
			<center><img src="<c:url value="resources/img/logo.png"/>" class="footer-logo"></center>
			<center><p class="light">CELTEN S.A | � 2016.</p></center>
		</div>
		<hr width="60%">

		<div class="footer2">
			<div class="row">

				<div class="col s4 scrollflow -pop " id="footer-message">
					<h5 class="white-text">�SOMOS CELTEN!</h5>
                	<p class="grey-text text-lighten-4">Este es un sitio para aprender cosas nuevas, para compartir, para hacer cosas grandiosas y lo m�s importante de todo, divertirse.</p>	
				</div>
				   
				<div class="col s4 center scrollflow -slide-top" id="links">
					<h5 class="white-text">�Quieres saber m�s?</h5>
		                <ul>
		                  <li><a class="grey-text text-lighten-3" href="">Conoce m�s de Celten</a></li>
		                  <li><a class="grey-text text-lighten-3" href="">Nuestros Servicios</a></li>
		                  <li><a class="grey-text text-lighten-3" href="">Nuestro Equipo</a></li>
		                </ul>	
				</div>

				<div class="col s4 center scrollflow -pop" id="contact">
					<div class="address">
						<i class="material-icons">room</i>
						<p class="light">Medell�n-Colombia</p>

					</div>
					<div class="telephone">
						<i class="material-icons">phone</i>
						<p class="light">+57 3136783501</p>
					</div>
					<div class="mail">
						<i class="material-icons">email</i>
						<p class="light">contacto@celtenSA.com.co</p>
					</div>
				</div>
		    </div>
		</div>
	</footer>
  	

	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		
	<script type="text/javascript" src="<c:url value="resources/js/global.js"/>"></script>	
   	<script type="text/javascript" src="<c:url value="resources/js/waves.js"/>"></script>
   	<script type="text/javascript" src="<c:url value="resources/js/leanModal.js"/>"></script>
   	<script type="text/javascript" src="<c:url value="resources/js/velocity.min.js"/>"></script>
   	<script type="text/javascript" src="<c:url value="resources/js/forms.js"/>"></script>
   	<script type="text/javascript" src="<c:url value="resources/js/collapsible.js"/>"></script>
   	
   	<script type="text/javascript" src="<c:url value="resources/js/index/load.components.js"/>"></script>
   	<script type="text/javascript" src="<c:url value="resources/js/index/scroll.down.js"/>"></script>
   	<script type="text/javascript" src="<c:url value="resources/js/index/modals.action.js"/>"></script>
   	<script type="text/javascript" src="<c:url value="resources/js/index/follow.twitter.js"/>"></script>	
   	
</body>
</html>