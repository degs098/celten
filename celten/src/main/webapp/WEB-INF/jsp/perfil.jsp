<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<!DOCTYPE html>
<html ng-app="celtenApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Mi Perfil - Celten S.A. | Celten</title>

	<link rel="stylesheet" type="text/css" href="<c:url value="resources/css/main.css"/>">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">	
	
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>	
	<script type="text/javascript" src="<c:url value="resources/angularResources/controllers/sideNavController.js"/>"></script>
</head>
<body>

	<jsp:include page="decorador/header.jsp"/>
	<jsp:include page="decorador/sideNav.jsp"/>

	<div class="onlineContainer col s4">										
			<ul class="collection onlineTitle">
				<li class="collection-item">
					<div>
						<span><strong>En L�nea</strong></span>
						<i class="material-icons tiny right blue-text text-darken-1">&#xE061;</i>
					</div>
				</li>
			</ul>
			<div class="onlineScroll">
				<ul class="collection onlineContent">
					<a class="dropdown-button" href="#!" data-activates="teamFrontend">					
						<li class="collection-item">
							<i class="grey-text text-darken-2 material-icons left onlineGroupTitle">&#xE3AE;</i>
							<span><strong>Equipo Frontend</strong></span>						
							<i class="grey-text text-darken-2 material-icons right">&#xE313;</i>	
							

							<ul id="teamFrontend" class="dropdown-content">
								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/caliche.jpg"/>">
									<span class="title">Andres Serna</span>							
								</li>
								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/samuel.jpg"/>">
									<span class="title">Samuel Vergara</span>
								</li>
							</ul>
						</li>
					</a>
					<a class="dropdown-button" href="#!" data-activates="teamBackend">
						<li class="collection-item">
							<i class="grey-text text-darken-2 material-icons left onlineGroupTitle">&#xE312;</i>
							<span><strong>Equipo Backend</strong></span>
							<i class="grey-text text-darken-2 material-icons right">&#xE313;</i>						
							
							<ul id="teamBackend" class="dropdown-content">					
								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/pernett.jpg"/>">
									<span class="title">Sebas Pernett</span>							
								</li>

								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/tolosita.jpg"/>">
									<span class="title">Juan Tolosa</span>
								</li>

								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/tolosita.jpg"/>">
									<span class="title">Juan Tolosa</span>
								</li>

								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/tolosita.jpg"/>">
									<span class="title">Juan Tolosa</span>
								</li>

								<li class="collection-item avatar">
									<img class="circle" src="<c:url value="resources/img/Perfil/tolosita.jpg"/>">
									<span class="title">Juan Tolosa</span>
								</li>
							</ul>
						</li>
					</a>
				</ul>
			</div>
		</div>	

	<div class="perfilContainer">
		<div class="main">
			<div class="row grey lighten-3">
				<div class="headerContainer col s12">
					<div class="iconDivContainer">
						<div class="iconDiv left">
							<i class="material-icons grey-text text-darken-2 circle small">&#xE853;</i>
						</div>					
						<p class="flow-text grey-text text-darken-2">						
							Mi Perfil
						</p>
					</div>					
				</div>				
			</div>	
			<div class="row">
				<div class="col s7 offset-s1 formContainer">		
					<form action="" method="POST">
						<div class="row">
							<div class="col colMargin">
								<div class="profileImage">
									<img src="<c:url value="data:image/jpeg;base64,${imagenPerfil}"/>" class="circle" id="imagePreview">
								</div>
								<div class="file-field input-field">
									<div class="btn waves-effect">
										<span><i class="material-icons left">&#xE2C6;</i>Subir</span>
										<input type="file" id="imagenPerfil" name="imagenPerfil" accept="image/*">
									</div>
								</div>					    	
							</div>
							<div class="col s7">
								<div class="row input-field">
									<input type="text" name="nombrePerfil" value="${usuario.nombre}">
									<label class="active" for="nombrePerfil">Nombre</label>	
								</div>										

								<div class="row input-field">
									<input type="text" name="apellidoPerfil" value="${usuario.apellido}">
									<label class="active" for="apellidoPerfil">Apellido</label>	
								</div>

								<div class="row input-field">
									<input disabled type="text" name="dniPerfil" value="${usuario.dni}">
									<label class="active" for="dniPerfil">C�dula</label>	
								</div>

								<div class="row input-field">
									<input type="number" name="edadPerfil" value="${usuario.edad}">
									<label class="active" for="edadPerfil">Edad</label>	
								</div>										
							</div>	
						</div>
						<div class="row">
							<div class="col s12 colMargin">
								<div class="row input-field">
									<input type="email" name="emailPerfil" value="${usuario.email}" class="validate">
									<label class="active" for="emailPerfil">Email</label>	
								</div>

								<div class="row doubleInput">
									<div class="col s6 input-field">
										<input disabled type="text" name="usernamePerfil" value="${usuario.usuario}">
										<label class="active labelSinLeft" for="usernamePerfil">Nombre de Usuario</label>
									</div>

									<div class="col s6 input-field">
										<input type="password" name="clavePerfil" value="${usuario.clave}">
										<label class="active" for="clavePerfil">Clave</label>
									</div>										
								</div>

								<div class="row input-field">
									<select id="teamPerfil" name="teamPerfil">
										<option value="" disabled selected>Elegir Equipo</option>
										<option value="frontend">Equipo Frontend</option>
										<option value="backend">Equipo Backend</option>										
									</select>
									<label>Equipo de trabajo</label>
								</div>

								<div class="row input-field">
									<select id="estadoPerfil" name="estadoPerfil">
										<option value="" disabled>Elegir Estado</option>
										<c:if test="${usuario.estado == true}">
											<option value="true" selected>Activo</option>
											<option value="false">De Vacaciones</option>
										</c:if>										
									</select>
									<label>�Activo o de Vacaciones?</label>
								</div>

								<div class="row input-field center">
									<a class="waves-effect waves-light btn">Guardar</a>
								</div>
							</div>
						</div>
					</form>		
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="<c:url value="resources/js/global.js"/>"></script>	
	<script type="text/javascript" src="<c:url value="resources/js/hammer.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/jquery.hammer.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/velocity.min.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/sideNav.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/waves.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/forms.js"/>"></script>
	<script type="text/javascript" src="<c:url value="resources/js/dropdown.js"/>"></script>

	<script type="text/javascript" src="<c:url value="resources/js/perfil/preview.image.js"/>"></script>

	<script type="text/javascript">
		jQuery(".button-collapse").sideNav();
		jQuery('.button-collapse').click(function(){
			this.sideNav('show');
		});

		$(document).ready(function() {
		    $('select').material_select();
		});
		
		function formSubmitLogout() {
			document.getElementById("logoutForm").submit();
		}		
	</script>
</body>
</html>