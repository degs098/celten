package com.celten.models;
import java.util.Date;
import org.springframework.stereotype.Component;

@Component
public class Tarea {
	
	private int id;	
	private String descripcion;
	private Date fecha_inicial;
	private Date fecha_final;
	private boolean estado;
	
	public Tarea(){
		
	}
	
	public Tarea(int id, String descripcion, Date fecha_inicial, Date fecha_final, boolean estado) {
		this.id = id;
		this.descripcion = descripcion;
		this.fecha_inicial = fecha_inicial;
		this.fecha_final = fecha_final;
		this.estado = estado;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Date getFecha_inicial() {
		return fecha_inicial;
	}
	
	public void setFecha_inicial(Date fecha_inicial) {
		this.fecha_inicial = fecha_inicial;
	}
	
	public Date getFecha_final() {
		return fecha_final;
	}
	
	public void setFecha_final(Date fecha_final) {
		this.fecha_final = fecha_final;
	}
	
	public boolean isEstado() {
		return estado;
	}
	
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
}
