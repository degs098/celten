package com.celten.models;
import java.sql.Blob;
import java.util.Date;
import org.springframework.stereotype.Component;

@Component
public class Usuario{
	
	private int id;
	private String usuario;
	private String clave;	
	private boolean enabled;
	private String dni;
	private String nombre;
	private String apellido;
	private int edad;
	private String email;
	private Date fechaNacimiento;
	private Blob imagenPerfil;
	
	public Usuario(){
		
	}

	public Usuario(int id, String usuario, String clave, boolean enabled, String dni, String nombre,
			String apellido, int edad, String email, Date fechaNacimiento, Blob imagenPerfil) {		
		this.id = id;
		this.usuario = usuario;
		this.clave = clave;
		this.enabled = enabled;
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.email = email;
		this.fechaNacimiento = fechaNacimiento;
		this.imagenPerfil = imagenPerfil;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean getEstado() {
		return enabled;
	}

	public void setEstado(boolean enabled) {
		this.enabled = enabled;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	public Blob getImagenPerfil() {
		return imagenPerfil;
	}

	public void setImagenPerfil(Blob imagenPerfil) {
		this.imagenPerfil = imagenPerfil;
	}		
		
}
