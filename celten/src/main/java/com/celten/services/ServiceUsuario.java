package com.celten.services;

import com.celten.models.Usuario;

public interface ServiceUsuario {

	public Usuario getUsuarioPorLogueoService();
	
	public String getImagenPerfil();
		
}
