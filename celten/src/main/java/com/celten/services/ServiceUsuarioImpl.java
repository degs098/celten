package com.celten.services;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Blob;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.celten.dao.DaoUsuario;
import com.celten.models.Usuario;

@Service
public class ServiceUsuarioImpl implements ServiceUsuario{

	@Autowired
	DaoUsuario daoUsuario;
	
	public Usuario getUsuarioPorLogueoService(){		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();		
		String nombreUsuario = auth.getName();
		Usuario user = daoUsuario.getUsuarioPorLogueoDao(nombreUsuario);
		return user;
	}

	public String getImagenPerfil(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();		
		String nombreUsuario = auth.getName();
		Blob image = daoUsuario.getImagenPerfil(nombreUsuario);
		File file = new File("C://xampp/htdocs/celten/imagenesPerfil/imagen.jpg");				
		byte b[] = null;
		String base64Encoded = "";		
		try {		
			FileOutputStream fos = new FileOutputStream(file);
			b = image.getBytes(1,(int) image.length());			
			b = Base64.encodeBase64(b);
			base64Encoded = new String(b, "UTF-8");
			fos.write(b);
			fos.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		
		return base64Encoded;
	}	

}
