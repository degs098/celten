package com.celten.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.celten.dao.DaoTarea;
import com.celten.models.Tarea;

@Service
public class ServiceTareaImpl implements ServiceTarea {
	
	@Autowired
	DaoTarea daoTarea;
	
	public ArrayList<Tarea> getTareasPorUsuarioService() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();		
		String nombreUsuario = auth.getName();
		ArrayList<Tarea> tasks = daoTarea.getTareasPorUsuarioDao(nombreUsuario);
		return tasks;
	}

}
