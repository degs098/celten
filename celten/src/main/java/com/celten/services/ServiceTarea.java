package com.celten.services;

import java.util.ArrayList;

import com.celten.models.Tarea;

public interface ServiceTarea {
	
	public ArrayList<Tarea> getTareasPorUsuarioService();
	
}
