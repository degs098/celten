package com.celten.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.celten.models.Usuario;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Repository
public class DaoUsuarioImpl implements DaoUsuario {

	@Autowired
	private DataSource dataSource;

	public Usuario getUsuarioPorLogueoDao(String nombreUsuario) {

		Usuario user = new Usuario();
		Connection conn = null;
		String sql = "SELECT ID, USUARIO, CLAVE, ENABLED, DNI, NOMBRE, APELLIDO, EDAD, EMAIL FROM USUARIOS WHERE USUARIO = '" + nombreUsuario + "'";

		try {
			conn = dataSource.getConnection();
			PreparedStatement p = conn.prepareStatement(sql);
			ResultSet r = p.executeQuery();						

			while (r.next()) {
				user.setId(r.getInt("id"));
				user.setUsuario(r.getString("usuario"));
				user.setClave(r.getString("clave"));
				user.setEstado(r.getBoolean("enabled"));
				user.setDni(r.getString("dni"));
				user.setNombre(r.getString("nombre"));
				user.setApellido(r.getString("apellido"));
				user.setEdad(r.getInt("edad"));
				user.setEmail(r.getString("email"));												
			}			
			p.close();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		return user;
	}
	
	public Blob getImagenPerfil(String nombreUsuario){
		Connection conn = null;
		String sql = "SELECT IMAGENPERFIL FROM USUARIOS WHERE USUARIO = '" + nombreUsuario + "'";		
		Blob image = null;		
		try {
			conn = dataSource.getConnection();
			PreparedStatement p = conn.prepareStatement(sql);
			ResultSet r = p.executeQuery();						
			while(r.next()){
				image = r.getBlob("imagenperfil");
			}
		} catch (Exception e) {
			System.out.println(e);
		}		
		return image;
	}

}
