package com.celten.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.celten.models.Tarea;

@Repository
public class DaoTareaImpl implements DaoTarea {
	
	@Autowired
	private DataSource dataSource;
	
	public ArrayList<Tarea> getTareasPorUsuarioDao(String nombreUsuario) {
		ArrayList<Tarea> tareas = new ArrayList<Tarea>();
		
		Connection conn = null;
		String sql = "SELECT tr.id, tr.descripcion, tr.fecha_inicial, tr.fecha_final, tr.estado FROM tareas AS tr INNER JOIN detalle_tareas AS dtr ON tr.id = dtr.id_tarea WHERE dtr.usuario = '" + nombreUsuario + "'";
		
		try {
			conn = dataSource.getConnection();
			PreparedStatement p = conn.prepareStatement(sql);
			ResultSet r = p.executeQuery();

			while (r.next()) {
				Tarea task = new Tarea();
				task.setId(r.getInt("id"));
				task.setDescripcion(r.getString("descripcion"));
				task.setFecha_inicial(r.getDate("fecha_inicial"));
				task.setFecha_final(r.getDate("fecha_final"));
				task.setEstado(r.getBoolean("estado"));
				tareas.add(task);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		
		return tareas;
	}

}
