package com.celten.dao;

import java.util.ArrayList;

import com.celten.models.Tarea;

public interface DaoTarea {
	
	public ArrayList<Tarea> getTareasPorUsuarioDao(String nombreUsuario);
	
}
