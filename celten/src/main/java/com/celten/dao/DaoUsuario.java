package com.celten.dao;

import java.sql.Blob;

import com.celten.models.Usuario;

public interface DaoUsuario {
	
	public Usuario getUsuarioPorLogueoDao(String nombreUsuario);
	
	public Blob getImagenPerfil(String nombreUsuario);
	
}
