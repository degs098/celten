package com.celten.controllers;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.celten.models.Tarea;
import com.celten.models.Usuario;
import com.celten.services.ServiceTarea;
import com.celten.services.ServiceUsuario;

@Controller
public class HomeController {

	@Autowired
	ServiceUsuario serviceUsuario;
	@Autowired
	ServiceTarea serviceTarea;
	
	@RequestMapping(value="/dashboard")
	public ModelAndView homePage(){
		ModelAndView model = new ModelAndView();
		model.setViewName("dashboard");

		String imageBase64 = serviceUsuario.getImagenPerfil();
		model.addObject("imagenPerfil", imageBase64);		
		
		Usuario user = serviceUsuario.getUsuarioPorLogueoService();
		model.addObject("nombreUsuario", user.getNombre() + " " + user.getApellido());
		model.addObject("email", user.getEmail());
		
		ArrayList<Tarea> tasks = serviceTarea.getTareasPorUsuarioService();
		model.addObject("tareasUsuario", tasks);
		
		return model;
	}	
	
	@RequestMapping(value="/perfil")
	public ModelAndView miPerfil(){
		ModelAndView model = new ModelAndView();
		model.setViewName("perfil");
		Usuario user = serviceUsuario.getUsuarioPorLogueoService();
		String imageBase64 = serviceUsuario.getImagenPerfil();
		model.addObject("usuario",user);
		model.addObject("imagenPerfil", imageBase64);
		return model;
	}
	
	@RequestMapping(value="/tareas")
	public ModelAndView misTareas(){
		ModelAndView model = new ModelAndView();
		model.setViewName("tareas");
		Usuario user = serviceUsuario.getUsuarioPorLogueoService();
		model.addObject("nombreUsuario", user.getNombre() + " " + user.getApellido());
		model.addObject("email", user.getEmail());
		return model;
	}	
	
	@RequestMapping(value="/getUsuarioPorLogueo")
	public @ResponseBody Usuario getDatosUsuarioLogueado(HttpServletRequest request, HttpServletResponse response){
		Usuario user = serviceUsuario.getUsuarioPorLogueoService();
		response.addHeader("Access-Control-Allow-Origin", "*");
		return user;
	}
	
}
