package com.celten.controllers;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {
	
	@RequestMapping(value={"/", "/index"})
	public String indexPage(ModelMap m){									
		return "index";
	}
		
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public ModelAndView loginPage(@RequestParam(value = "error",required = false) String error,
	@RequestParam(value = "logout",	required = false) String logout) {		
		ModelAndView model = new ModelAndView();
		model.setViewName("index");			
		
		if(error != null){
			model.addObject("error", "�Oops! Usuario o Clave incorrectos");
		}
		if(logout != null){
			model.addObject("logoutMessage", "�Bien! Has cerrado sesi�n correctamente");
		}
		
		return model;
	}
	
	@RequestMapping(value="admin")
	public ModelAndView adminPage(){
		ModelAndView model = new ModelAndView();
		model.setViewName("admin");
		return model;
	}	
	
	@RequestMapping(value="/404")
	public ModelAndView noFoundPage(){
		ModelAndView model = new ModelAndView();
		model.setViewName("404");
		return model;
	}
	
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

	  ModelAndView model = new ModelAndView();
	  
	  //Verifica si el usuario esta logueado
	  Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	  
	  if (!(auth instanceof AnonymousAuthenticationToken)) {
		UserDetails userDetail = (UserDetails) auth.getPrincipal();	
		model.addObject("username", userDetail.getUsername());
	  }
	  model.setViewName("403");
	  return model;
	}
	
	@RequestMapping(value="nuestro-equipo")
	public ModelAndView teamPage(){
		ModelAndView model = new ModelAndView();
		model.setViewName("nuestro-equipo");
		return model;
	}		
	
}
