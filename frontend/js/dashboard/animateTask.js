jQuery(document).ready(function(){
	var timer;
	jQuery(".taskContainer a").on("mousedown",function(){
		var oID = jQuery(this).attr("id");
		var check = jQuery("#"+oID+" span:nth-child(2) i:nth-child(1)");
		var close = jQuery("#"+oID+" span:nth-child(2) i:nth-child(2)");

		timer = setTimeout(function(){

			if (check.is(':visible')) {
				check.hide(500);
				close.show(1000);
			} else {
				check.show(1000);
				close.hide(500);
			}
			
		},300);
	}).on("mouseup mouseleave",function(){
		clearTimeout(timer);
	});
});